﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelMT
{
    internal sealed class RoomDetail
    {
        /* ความคิดเห็น  :
         *      1. กำหนดให้คลาส BookDetail นี้ จะใช้เป็น data class เท่านั้น จึงมีแค่ get;set; 
         *         ไม่มี method อื่น เพื่อความสะดวกรวดเร็ว
         *      2. ด้วยเหตุผลข้อ 1 จึงบังคับเป็น sealed (ห้ามสืบทอด)
         *      3. ทุกชื่อ สื่อความหมายอยู่แล้ว แต่เฉพาะ RoomTxt เพิ่มเข้ามาเพราะ มีการขอเลขห้องในรูปแบบ
         *         string ค่อนข้างบ่อย จึงทำ property เป็น getter เพิ่มขึ้นมา
         *      
         */
        private int _roomNo;
        private int _keycardId;
        private string _roomTxt = "";
        private Guest? _guest;
        private DateTime _created;

        public int Room {
            get { return _roomNo; }
            set { 
                _roomNo = value;
                _roomTxt = _roomNo.ToString();
            }
        }
        public int KeycardId { get => _keycardId; set => _keycardId = value; }
        public string RoomTxt { get => _roomTxt; }
        public Guest? GuestOwner { get => _guest; set => _guest = value; }
        public DateTime Created { get => _created; set => _created = value; }
    }
}
