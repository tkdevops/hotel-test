﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelMT
{
    /*
     * ความคิดเห็น  : 
     *      - คลาส TheKey ความจริงควรเป็นส่วนหนึ่งของ Hotel (ให้ทำเป็น composition (has-a)) 
     * แต่ในที่นี้ ผมตั้งใจให้ทำงานเป็นลักษณะแยก เพราะจะแสดงการทำงานเทคนิคเขียนโปรแกรมและ OOP ส่วนที่เหลือ
     * เท่าที่จะจัดให้ลงได้ เช่น การ initialize ค่าด้วย static block, static method เป็นต้น 
     * โดยให้ตัวคลาสทำงานเป็นแบบ Sigleton
     * 
     *      - อีกเรื่องที่สำคัญคือ สาเหตุที่ใช้ List มาเก็บคู่ค่า Tuple<keyid, inuse> แทนที่จะใช้ List
     * ไปตรงๆ (ซึ่งก็ทำงานได้เหมือนกันแถมเร็วกว่า) ก็เพราะ "เลขห้องและเลขอื่นๆ ส่วนใหญ่ในโปรแกรมนี้ เริ่มต้นที่เลข 1"
     * จึงทำเป็นคู่คีย์ (เริ่มที่เลข 1 โดยไม่สนใจ index) เพื่อช่วยป้องกันความสับสน และลดการเกิดข้อผิดพลาด
     * (สังเกตุว่า เลขห้อง, เลขชั้น เฉพาะของโปรเจ็คนี้ ก็จะใช้หลักการนี้เหมือนกัน โดยจะตัดทิ้งหรือไม่ใช้ index 0)
     * 
     * หลักการทำงาน :
     *      - ถ้าไม่มีกุญแจ ก็ให้สร้างใหม่สำหรับห้องพักนั้น
     *      - แต่ถ้ามีอยู่ แต่ว่าง (เพราะได้รับการ check-out จากห้องอื่น) ให้ใช้ key ที่มีค่าน้อยสุดก่อน (โจทย์บังคับ)
     */
    internal class TheKey
    {
        private static List<(int keyid, bool inuse)> m_keys = new();

        static TheKey() {
            m_keys.Add( (1, false) );
        }


        /// <summary>
        /// ขอเลข KeycardId โดยจะคืนค่าเลขที่น้อยที่สุด ที่ไม่มีการใช้งานก่อนเสมอ 
        /// </summary>
        /// <returns>ค่า KeycardId ที่ไม่มีใครใช้</returns>
        public static int GetKeycardId()
        {
            int maxkey = 0;
            for(int i = 0; i < m_keys.Count; i++)
            {
                maxkey = m_keys[i].keyid > maxkey ? m_keys[i].keyid : maxkey;
                if (!m_keys[i].inuse)
                {
                    m_keys[i] = (m_keys[i].keyid, true);
                    return m_keys[i].keyid;
                }
            }
            m_keys.Add((maxkey + 1, true));
            return maxkey + 1;
        }

        /// <summary>
        /// ยกเลิก KeycardId
        /// </summary>
        /// <param name="keyid">หมายเลขคีย์ที่ยกเลิก</param>
        public static void ReturnKeycardId(int keyid)
        {
            if (m_keys.Contains( (keyid, true) ))
            {
                var i = m_keys.IndexOf((keyid, true));
                m_keys[i] = (keyid, false);
            }
        }
    }
}
