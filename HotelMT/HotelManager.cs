﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelMT
{
    internal class HotelManager
    {
        /*
         * คลาส HotelManager :
         *         เป็นคลาสที่จะเรียกคลาสอื่นๆ มากระทำการทั้งหมด โดยเริ่มจากโหลด file script ชุดคำสั่งต่างๆ แล้ว
         *     เอาไปเก็บไว้ที่ m_scrtipt แล้วไปเรียก m_strCmd ให้มาทำงาน (ตามที่ได้ map กับ action method ต่างๆ)
         *     
         * หมายเหตุ :
         *     สำหรับไฟล์นี้มี method หลักอันเดียว  "ผมจึงตัดสินใจไม่ใส่" comments เอาไว้ในแต่ละ method (ใส่เฉพาะที่สำคัญเท่านั้น)
         *     เพราะต้องการให้ clean สะอาด ไม่ดูรก เพื่อให้ผู้เกี่ยวข้อง ได้ตรวจลักษณะ code style, แนวคิด และอื่นๆ โดยง่ายครับ
         *  
         *           
         */
        private Hotel? hotel;
        private List<(string cmd, string parameters)> m_script;        
        private Dictionary<string, HotelAction> m_strCmd;
        private delegate void HotelAction(string strParams);

        public HotelManager(string strScriptFilename)
        {
            // Caution !! คำอธิบายด้านล่าง
            hotel = null;

            /*
             * เก็บชุด script คำสั่งทั้งหมด แล้วชี้ไปที่ method ที่ทำงานนั้น ๆ
             */
            m_strCmd = new Dictionary<string, HotelAction>();
            m_strCmd.Add("create_hotel", ActionCreateHotel);
            m_strCmd.Add("book", ActionBookHotel);
            m_strCmd.Add("list_available_rooms", ActionListAvailableRooms);
            m_strCmd.Add("checkout", ActionCheckout);
            m_strCmd.Add("list_guest", ActionListGuest);
            m_strCmd.Add("get_guest_in_room", ActionGetGuestInRoom);
            m_strCmd.Add("list_guest_by_age", ActionListGuestByAge);
            m_strCmd.Add("list_guest_by_floor", ActionListGuestByFloor);
            m_strCmd.Add("checkout_guest_by_floor", ActionCheckoutGuestByFloor);
            m_strCmd.Add("book_by_floor", ActionBookByFloor);

            m_script = new List<(string, string)>();

            LoadFile( strScriptFilename );
        }

        /// <summary>
        /// เริ่มดำเนินการตาม script ที่วางไว้
        /// </summary>
        public void RunScript()
        {
            foreach (var (cmd, parameters) in m_script)
            {
                if (m_strCmd.ContainsKey(cmd))
                {
                    m_strCmd[cmd](parameters);
                }
            }
        }

        /*
         * 
         * ข้อควรระวัง !! : 
         * จะสังเกตุเห็นว่า object "hotel" จะเริ่ม initialize หลังจากคำสั่ง createHotel นี้
         * ดังนั้น ถ้าไม่มีคำสั่ง createhotel นี้ใน script แล้วไปเรียกใช้คำสั่งอื่นเลย ก็จะ error อย่างแน่นอน
         * ซึ่งทางแก้ก็จะเป็นวิธีใด วิธีหนึ่ง ในต่อไปนี้ เช่น
         *      - ถ้าไม่มีคำสั่งสร้าง hotel ก็กำหนด default ชั้นและห้องของโรงแรมไปเลย 
         *            (บังคับว่าจะไม่ null แน่นอน)
         *      - หรือ ประกาศ hotel เป็น nullable แล้วดักเอาไว้ทุกที่ ซึ่งอาจจะผ่านพ้นไป แต่ทำงานต่อไม่ได้ 
         *            (รู้ว่า null แต่รับผิดชอบเท่าที่มีในขอบเขต)
         *      - หรือ ไม่ดักทุกที่  แต่ปล่อยให้ (หรือ Throw) exception ออกมาแล้วไปดักที่ต้นทาง 
         *            (คือปล่อย null ออกไป เพื่อให้บอกไปถึงต้นทาง หรืออาจจะเป็น user)
         *      - หรือ ให้โปรแกรมเมอร์ที่รับผิดชอบ เป็นคนตรวจ (ครอบ try/catch ไปเลย) 
         *            (วิธีนี้เหมือนง่ายสุด แต่อย่าลืมว่า script มันผิดเพราะไม่มีการสร้าง hotel 
         *            การทำข้อนี้ เหมือนวิธีซุกปัญหาใต้พรม มันไม่สะท้อนไปที่ต้นทางให้แก้ไข script)
         *      - ฯลฯ
         * 
         * ความเห็น : ควรจะปรึกษากันภายในว่าในทีมจะใช้วิธีใด ในที่นี้ใช้วิธีที่ 1 และ 2 ร่วมกัน
         * 
         * 
         * ส่วนถัดจากนี้ไปเป็น action ที่ reference ไปที่ชุดคำสั่งของคลาส Hotel
         */
        private void ActionCreateHotel(string strParams)
        {
            // parameter : [total_floor] [total_room]
            string[] paralists = strParams.Split(' ');
            hotel = new Hotel( int.Parse(paralists[0]), int.Parse(paralists[1]) );
        }
        private void ActionBookHotel(string strParams)
        {
            // parameter : [room] [name] [age]
            string[] paralists = strParams.Split(' ');
            Guest guest = new Guest( paralists[1], int.Parse(paralists[2]) );

            hotel ??= new Hotel(2, 3);
            hotel.BookingRoom(int.Parse(paralists[0]), guest,out string strResult);
            Console.WriteLine(strResult);
        }

        private void ActionListAvailableRooms(string strParams)
        {
            // no paramter :
            hotel ??= new Hotel(2, 3);
            string strOutput = hotel.ListAvailableRooms();
            Console.WriteLine(strOutput);
        }

        private void ActionCheckout(string strParams)
        {
            // parameter : [keycardId] [name]
            string[] paralists = strParams.Split(' ');

            hotel ??= new Hotel(2, 3);
            hotel.CheckOut( int.Parse(paralists[0]), paralists[1], out string strOutput);
            Console.WriteLine(strOutput);
        }

        private void ActionListGuest(string strParams)
        {
            // no paramter :
            hotel ??= new Hotel(2, 3);
            string strOutput = hotel.ListGuest();
            Console.WriteLine(strOutput);
        }

        private void ActionGetGuestInRoom(string strParams)
        {
            // parameter : [roomNo]
            string []paralists = strParams.Split(' ');

            hotel ??= new Hotel(2, 3);
            string strOutput = hotel.GetGuestInRoom( int.Parse(paralists[0]));
            Console.WriteLine(strOutput);
        }

        private void ActionListGuestByAge(string strParams)
        {
            // parameter : [conditionSign] [age]
            string[] paralists = strParams.Split(' ');

            hotel ??= new Hotel(2, 3);
            string strOutput = hotel.ListGuestByAge( paralists[0], int.Parse(paralists[1]) );
            Console.WriteLine(strOutput);
        }

        private void ActionListGuestByFloor(string strParams)
        {
            // parameter : [floor]
            string[] paralists = strParams.Split(' ');

            hotel ??= new Hotel(2, 3);
            string strOutput = hotel.ListGuestByFloor( int.Parse(paralists[0]) );
            Console.WriteLine(strOutput);
        }

        private void ActionCheckoutGuestByFloor(string strParams)
        {
            // parameter : [floor]
            string[] paralists = strParams.Split(' ');

            hotel ??= new Hotel(2, 3);
            string strOutput = hotel.CheckOutGuestByFloor( int.Parse(paralists[0]) );
            Console.WriteLine(strOutput);
        }

        private void ActionBookByFloor(string strParams)
        {
            // parameter : [floor] [guestName] [age]
            string[] paralists = strParams.Split(' ');
            Guest guest = new Guest( paralists[1], int.Parse(paralists[2]) );

            hotel ??= new Hotel(2, 3);
            string strOutput = hotel.BookByFloor(int.Parse(paralists[0]), guest);
            Console.WriteLine(strOutput);
        }   

        private void LoadFile(string strFilename)
        {
            try
            {
                StreamReader sr = new StreamReader(strFilename);
                while (sr.Peek() != -1)
                {
                    string tmpString = sr.ReadLine()!.Replace("  ", " ").Trim();
                    string tmpCmd = "";
                    string tmpParams = "";
                    if (tmpString.Length > 0 && tmpString.Contains(' '))
                    {
                        tmpCmd = tmpString[..tmpString.IndexOf(' ')];
                        tmpParams = tmpString[(tmpString.IndexOf(' ') + 1)..];
                    }
                    else
                    {
                        tmpCmd = tmpString;
                        tmpParams = "";
                    }
                    m_script.Add( (tmpCmd, tmpParams) );
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("error.. : LoadFile\ndetails : {0}", ex.Message);
            }
        }        
    }
}
