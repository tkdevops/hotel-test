﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelMT
{
    internal class Hotel
    {
        /*
         * คลาส Hotel :
         *           รวบรวม method เท่าที่จำเป็นสำหรับการกระทำหรือดำเนินงานต่าง ๆ 
         *           คลาสนี้ ผมใส่ comment ตามแบบของ microsoft
         *           
         *           ในไฟล์นี้ ผมพยายามแสดง coding style ที่จะเน้นไปการย่อยโค้ดไม่ให้ยาวไป, อะไรที่ยาวไป
         *           ก็จะประกาศตัวแปรมารองรับ (ทำให้สั้นลง) จึงอาจจะมีการเห็นประกาศตัวแปรทิ้งขว้างกระจายไปทั่วโค้ด 
         *           (หรือก็คือ ตัวแปรใช้แล้วทิ้ง ในที่นี้มีจุดสังเกตุจะประกาศด้วย var), สำหรับตัว method ก็จะพยายาม
         *           ไม่ให้มีหลายบรรทัดเกินไป ถ้ายาวเกินก็แตกเป็น function/method อื่นหรือทำเป็น delegate
         *           และจะเขียนไปพร้อมๆ กับให้ความคิดเห็น ว่าทำไมต้องเป็นแบบนั้น อย่างนั้น เป็นระยะๆ 
         *            
         * m_rooms : เก็บจำนวนห้องที่มี และแขกผู้เข้าพัก
         * ความเห็น  : ที่จริงโดยทั่วไป งานที่กำหนดลักษณะตายตัวแบบนี้  ควรการเก็บเป็น 2มิติ (ยิ่งถ้าเป็นการจัดการข้อมูลมากๆ 
         *           ระดับ n แถว และ m คอลัมน์จะเห็นผลชัด) แต่ในที่นี้ เป็นแค่โรงแรมเก็บเลขห้องมี 3 หลัก (ซึ่งไม่เกิน 
         *           9 ชั้น ชั้นละไม่เกิน 99 ห้อง) จึงตัดสินใจใช้ Collection (Dictionary) เพราะเลขห้องไม่ซ้ำกันอยู่แล้ว 
         *           และสามารถเขียนเพื่อแสดง Proficiency อื่นๆ ได้หลากหลายท่ามากกว่า
         */
        private Dictionary<int, RoomDetail> m_rooms = new();

        /* เงื่อนไขโจทย์บังคับ:
         *           1. สร้างโรงแรมโดยกำหนดจำนวนชั้นและจำนวนห้องต่อชั้นได้
         *           เลขห้องจะต้องเป็นเลข 3 หลัก ตัวแรกคือเลขชั้น 2 ตัวที่เหลือคือเลขห้อง เริ่มจาก 01
         * ความเห็น  :
         *           ที่จริงแล้วควรใช้ byte เพราะเหมาะสมกับคุณค่าของตัวแปร (ที่มีค่าแค่ 1-99) 
         *           แต่ยุคปัจจุบัน บางครั้งมันก็ไม่ได้ช่วยประหยัดหน่วยความจำไปกว่าการใช้ int แล้ว หรือบางกรณีอาจจะเพิ่มภาระและทำงานได้ล่าช้ากว่า int ด้วย 
         *           (เพราะ CPU/VM ต้องแบ่งการทำงานเป็น 8 bit แทนที่จะเป็น 64 bit) 
         *           ในที่นี้จึงตัดสินใจใช้ int ในการเก็บเลขห้อง,ชั้น (ทั้ง totalFloor และ totalRoom) เพราะมีประสิทธิภาพ และสะดวกกว่า 
         *           การเลือกใช้ type ที่เป็น byte (0-255) ก็ถือเป็นการจำกัดข้อผิดพลาดที่ดี (เพราะจะไม่มีค่าติดลบ (จำนวนชั้น/ห้องติดลบไม่ได้)) แต่ที่นี้ละเอาไว้
         *           
         */

        /// <summary>
        /// ใช้สร้างห้องพักว่างๆ ตามจำนวนชั้นและห้อง
        /// โดยจะได้ห้องโล่งๆ ที่ยังไม่มี Guest เข้าพัก
        /// </summary>
        /// <param name="totalFloor">จำนวนชั้น 1-9</param>
        /// <param name="totalRoom">จำนวนห้องแต่ละชั้น 1-99</param>
        public Hotel(int totalFloor, int totalRoom)
        {
            /* ความคิดเห็น :
             * - floor จะต้องเป็น 1->9  และ room 1->99 เท่านั้นเพราะโจทย์กำหนดให้มีได้เต็มที่ 3 หลัก
             * - ปกติจะเขียนบรรทัดเดียวแบบนี้ (เพราะรู้สึกยังยาวไม่มาก)
             * int initFloor = totalFloor > 9 ? 9 : totalFloor <= 0 ? 1 : totalFloor;
             * int initRoom = totalRoom > 99 ? 99 : totalRoom <= 0 ? 1 : totalRoom;
             *             
             * - แต่เฉพาะในตอนทำแบบทดสอบนี้ จึงเขียนแบบนี้ (2 condition)
             */
            int initFloor = totalFloor > 9 ? 9 
                : totalFloor <= 0 ? 1 
                : totalFloor;
            int initRoom = totalRoom > 99 ? 99 
                : totalRoom <= 0 ? 1 
                : totalRoom;

            for (int floor = 1; floor <= initFloor; floor++)
            {
                for (int room = 1; room <= initRoom; room++)
                {
                    RoomDetail roomDetail = new RoomDetail();
                    roomDetail.Room = floor * 100 + room;
                    roomDetail.GuestOwner = null;
                    roomDetail.KeycardId = 0;
                    m_rooms.Add( roomDetail.Room, roomDetail );
                }
            }
            Console.WriteLine( "Hotel created with {0} floor(s), {1} room(s) per floor."
                , initFloor, initRoom );
        }

        /// <summary>
        /// ทำการจองห้องพัก
        /// </summary>
        /// <param name="roomNo">หมายเลขห้อง</param>
        /// <param name="guest">รายละเอียดแขกผู้เข้าพัก</param>
        /// <param name="result">ข้อความที่แสดงผลการทำงาน</param>
        /// <returns>true ถ้าหากว่าจองได้สำเร็จ, นอกนั้น false</returns>
        public bool BookingRoom(int roomNo, Guest guest, out string result)
        {
            result = "";
            if (guest is null)
            {
                result = string.Format("Guest cannot set to null.");
                return false;
            }

            if (m_rooms.ContainsKey(roomNo))
            {
                if (m_rooms[roomNo].GuestOwner is null)
                {
                    m_rooms[roomNo].GuestOwner = guest;
                    m_rooms[roomNo].KeycardId = TheKey.GetKeycardId();
                    m_rooms[roomNo].Created = DateTime.Now;
                    result = string.Format("Room {0} is booked by {1} with keycard number {2}."
                        , roomNo, guest.Name, m_rooms[roomNo].KeycardId);
                    return true;
                }
                else
                {
                    result = string.Format("Cannot book room {0} for {1}, "
                        + "The room is currently booked by {2}."
                        , roomNo, guest.Name, m_rooms[roomNo].GuestOwner!.Name );
                    return false;
                }
            }
            result = string.Format( "Room No.{0} not found.", roomNo );
            return false;
        }

        /* 
         * เงื่อนไขโจทย์บังคับ :  2. ดูรายการห้องว่างทั้งหมดได้
         */
        /// <summary>
        /// แสดงรายการว่าห้องพักใดกำลังว่าง
        /// </summary>
        /// <returns>รายการห้องที่ว่างคั่นด้วย , </returns>
        public string ListAvailableRooms()
        {
            string result = "";
            foreach (RoomDetail roomDetail in m_rooms.Values)
            {
                result += roomDetail.GuestOwner is null 
                    ? roomDetail.RoomTxt + ", " 
                    : "";
            }
            result = result.Length > 2 
                ? result.Remove(result.Length - 2) 
                : "<no room available>";
            return result;
        }

        /* 
         * เงื่อนไขโจทย์บังคับ : 3. ดูรายชื่อแขกทั้งหมดได้
         */

        /// <summary>
        /// ดูรายชื่อแขกที่เข้าพักทั้งหมด
        /// </summary>
        /// <returns>รายชื่อลูกค้าที่เข้าพักคั่นด้วย , </returns>
        public string ListGuest()
        {
            string result = "";
            var allRooms = m_rooms.Values.OrderBy(x => x.Created);
            foreach (RoomDetail roomDetail in allRooms)
            {
                result += roomDetail.GuestOwner != null 
                    ? roomDetail.GuestOwner.Name + ", " 
                    : "";
            }
            result = result.Length > 2 
                ? result.Remove(result.Length - 2) 
                : "<no room available>";
            return result;
        }

        /* 
         * เงื่อนไขโจทย์บังคับ : 4. ดูรายชื่อแขกโดยกำหนดช่วงอายุได้
         * 
         * ความเห็น : 
         *        ความจริงเงื่อนไขสั้นๆ แบบนี้ ใช้ switch/case ก็จะเขียนได้เร็วกว่า (แต่ก็อาจจะทำให้ method นี้ยาวนิดหน่อย)
         *        แต่กังวลว่าอาจจะไม่ตอบโจทย์ (ที่ผู้เกี่ยวข้อง ต้องการดูการเทคนิคการเขียนโปรแกรม, oop, ความเข้าใจเป็นหลัก) 
         *        
         *        ในที่นี้จึงเลือกใช้ delegate มา implements เงื่อนไข 6 แบบ <, <=, >, >=, =, == 
         *        โดยเก็บชุดคำสั่งไว้ใน dictionary แล้วเรียกใช้ถ้าหากว่าตรงเงื่อนไข
         *          
         */
        /// <summary>
        /// ดูรายชื่อแขกแบบกำหนดช่วงอายุได้
        /// </summary>
        /// <param name="conditionSign">เงื่อนไขที่ตรวจสอบ</param>
        /// <param name="age">อายุที่ตรวจสอบ</param>
        /// <returns>รายชื่อแขกที่มีอายุตรงตามเงื่อนไข</returns>
        public string ListGuestByAge(string conditionSign, int age)
        {
            /*
             * ชุดคำสั่ง delegate ถ้าจะให้ดี สามารถแยกไปเป็นอีก class ได้ และ/หรือกำหนด static เพื่อให้
             * ไม่ต้อง initial ค่าใหม่ทุกครั้ง แต่การทำอย่างนั้นมันจะทำให้โปรแกรมนี้มีขนาดใหญ่เกินไป (รกไป)
             */

            Dictionary<string, checkAge> checkAgeCmd = new Dictionary<string, checkAge>();
            checkAgeCmd.Add( "<", CheckAgeLT );
            checkAgeCmd.Add( ">", CheckAgeGT );
            checkAgeCmd.Add("<=", CheckAgeLE );
            checkAgeCmd.Add(">=", CheckAgeGE );
            checkAgeCmd.Add( "=", CheckAgeEQ );
            checkAgeCmd.Add("==", CheckAgeEQ );

            string result = "";
            foreach (RoomDetail roomDetail in m_rooms.Values)
            {
                if (roomDetail.GuestOwner != null)
                {
                    if (checkAgeCmd.ContainsKey(conditionSign))
                    {
                        result += checkAgeCmd[conditionSign]( roomDetail.GuestOwner, age );
                    }
                } 
            }
            result = result.Length > 2 
                ? result.Remove(result.Length - 2) 
                : "<no man to show>";
            return result;
        }

        /// <summary>
        /// ชุดคำสั่ง delegate ใช้สำหรับตรวจหาอายุแบบมีเงื่อนไข
        /// </summary>
        /// <param name="guest">เก็บรายละเอียดผู้ที่เข้าพัก</param>
        /// <param name="chkAgeCondition">อายุที่เป็นเงื่อนไข</param>
        /// <returns>รายชื่อผู้เข้าพักกรณีที่ตรงตามเงื่อนไข</returns>
        private delegate string checkAge(Guest guest, int chkAgeCondition);
        private string CheckAgeLT(Guest guest, int chkAgeCondition) => guest.Age < chkAgeCondition ? guest.Name + ", " : "";
        private string CheckAgeGT(Guest guest, int chkAgeCondition) => guest.Age > chkAgeCondition ? guest.Name + ", " : "";
        private string CheckAgeLE(Guest guest, int chkAgeCondition) => guest.Age <= chkAgeCondition ? guest.Name + ", " : "";
        private string CheckAgeGE(Guest guest, int chkAgeCondition) => guest.Age >= chkAgeCondition ? guest.Name + ", " : "";
        private string CheckAgeEQ(Guest guest, int chkAgeCondition) => guest.Age == chkAgeCondition ? guest.Name + ", " : "";

        /* 
         * เงื่อนไขโจทย์บังคับ : 5. ดูชื่อแขกในห้องพักที่ระบุได้
         */
        /// <summary>
        /// ตรวจสอบรายชื่อแขก ตามเลขห้องพักที่ระบุ
        /// </summary>
        /// <param name="roomNo"></param>
        /// <returns>รายชื่อแขกในห้องพักที่ระบุ</returns>
        public string GetGuestInRoom(int roomNo)
        {
            string result = ( m_rooms.ContainsKey(roomNo) && m_rooms[roomNo].GuestOwner != null )
                ? m_rooms[roomNo].GuestOwner!.Name 
                : "<no man in room>";
            return result;
        }

        /* 
         * เงื่อนไขอื่นๆ ที่โจทย์ไม่ได้ระบุ แต่มีใน input.txt
         */
        /// <summary>
        /// แขกคืนห้องพัก
        /// </summary>
        /// <param name="keyNo">หมายเลขห้อง</param>
        /// <param name="guestName">ชื่อของแขกเจ้าของห้อง</param>
        /// <param name="strOutput">ข้อความผลลัพธ์</param>
        /// <returns>คืนค่า true หากไม่มีข้อผิดพลาด นอกนั้นเป็น false</returns>
        public bool CheckOut(int keyNo, string guestName, out string strOutput)
        {
            strOutput = "";
            var allRooms = m_rooms.Values.OrderBy(x => x.Created);
            foreach (RoomDetail roomDetail in allRooms )
            {
                if ( roomDetail.KeycardId == keyNo && roomDetail.GuestOwner != null) 
                {
                    if ( roomDetail.GuestOwner!.Name.Equals(guestName) )
                    {
                        strOutput = string.Format("Room {0} is checkout.", roomDetail.RoomTxt);
                        roomDetail.GuestOwner = null;
                        TheKey.ReturnKeycardId( roomDetail.KeycardId );
                        return true;
                    }
                    else
                    {
                        strOutput = string.Format("Only {0} can checkout with keycard number {1}."
                            , roomDetail.GuestOwner!.Name, roomDetail.KeycardId);
                        return false;
                    }
                }
            }
            strOutput = "something wrong";
            return false;
        }

        /// <summary>
        /// แสดงรายชื่อแขกทั้งชั้นที่ระบุ
        /// </summary>
        /// <param name="floorNo">หมายเลขชั้น</param>
        /// <returns>คืนค่ารายชื่อแขกที่เข้าพักคั่นด้วย ,</returns>
        public string ListGuestByFloor(int floorNo)
        {
            string result = "";
            var allRooms = m_rooms.Values.OrderBy(x => x.Created);
            foreach (RoomDetail roomDetail in allRooms)
            {
                if (roomDetail.GuestOwner != null)
                {
                    var tmpFloor = int.Parse(roomDetail.RoomTxt[0].ToString());
                    if (tmpFloor == floorNo )
                    {
                        result += roomDetail.GuestOwner.Name + ", ";
                    }
                }
            }
            result = result.Length > 2
                ? result.Remove(result.Length - 2)
                : "<no man in this floor>";
            return result;
        }

        /// <summary>
        /// ตรวจสอบว่าทั้งชั้นระบุ มีห้องว่างหรือไม่
        /// </summary>
        /// <param name="floorNo">หมายเลขชั้น</param>
        /// <returns>คืนค่า true หากชั้นนั้นว่างทุกห้อง นอกนั้นเป็น false</returns>
        private bool IsEmptyFloor(int floorNo)
        {
            foreach (RoomDetail roomDetail in m_rooms.Values)
            {
                var tmpFloor = int.Parse(roomDetail.RoomTxt[0].ToString());
                if (tmpFloor == floorNo && roomDetail.GuestOwner != null) {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// จองห้องพักแบบเหมาทั้งชั้น
        /// </summary>
        /// <param name="floorNo">หมายเลขชั้น</param>
        /// <param name="guest">รายละเอียดแขกผู้เข้าพัก</param>
        /// <returns>ข้อความผลลัพธ์รายละเอียดการจองห้องพัก</returns>
        public string BookByFloor(int floorNo, Guest guest)
        {
            if ( ! IsEmptyFloor(floorNo) )
            {
                return string.Format("Cannot book floor {0} for {1}"
                    , floorNo, guest.Name);
            }

            string roomsList = "";
            string keysList = "";
            var allRooms = m_rooms.Values.OrderBy(d => d.Created);
            foreach ( RoomDetail roomDetail in allRooms) 
            {
                var tmpFloor = int.Parse(roomDetail.RoomTxt[0].ToString());
                if (tmpFloor == floorNo)
                {
                    if (BookingRoom(roomDetail.Room, guest, out string strOutput))
                    {
                        roomsList += roomDetail.RoomTxt + ", ";
                        keysList += roomDetail.KeycardId.ToString() + ", ";
                    }
                }
            }

            if ( roomsList.Length > 2)
            {
                roomsList = roomsList.Remove(roomsList.Length - 2);
                keysList = keysList.Remove(keysList.Length - 2);

                string strOutput = string.Format("Room {0} are booked with keycard number {1}"
                    , roomsList
                    , keysList );
                return strOutput;
            }

            return string.Format("Cannot book floor {0} for {1}.", floorNo, guest.Name);
        }


        public string CheckOutGuestByFloor(int floorNo)
        {
            string strResult = "";
            foreach (RoomDetail roomDetail in m_rooms.Values)
            {
                var tmpFloor = int.Parse(roomDetail.RoomTxt[0].ToString());
                if (tmpFloor == floorNo && roomDetail.GuestOwner != null)
                {
                    int keycardId = roomDetail.KeycardId;
                    string guestName = roomDetail.GuestOwner.Name;

                    strResult += CheckOut( keycardId, guestName, out string strOutput )
                        ? roomDetail.RoomTxt + ", "
                        : "";
                }
            }
            strResult = strResult.Length > 2
                ? "Room " + strResult.Remove(strResult.Length - 2) + " are checkout."
                : "<no one checkout in this floor>";
            return strResult;
        }

    }
}
