﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelMT
{
    internal class Guest : Person
    {
        /*
         * โปรดดูคำอธิบายที่ละเอียด จากไฟล์ Person.cs (คลาส Person)
         * 
         */
        public Guest(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }
    }
}
