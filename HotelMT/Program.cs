﻿using System;

namespace HotelMT
{
    internal class Program
    {
        static void Main(string[] args)
        {
            HotelManager mgr = new HotelManager("input.txt");
            mgr.RunScript();

            Console.WriteLine("- End -");
        }
    }
}